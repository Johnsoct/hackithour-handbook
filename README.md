# **Company Handbook**

* [Technologies Used](#technologies-used)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installing](#installing)
  * [Downloading the Code](#downloading-the-code)
* [Creating A New Branch for Updating](#create-a-new-branch-for-updating)
* [Making Changes](#making-changes)
* [Creating New Files](#creating-new-files)
* [Ensure Remote Origin is Setup](#ensure-remote-origin-is-setup)
* [Pushing Changes Back to Repository](#pushing-changes-back-to-repository)
* [Requesting a Merge Request on GitLab.com](#requesting-a-merge-request-on-gitlabcom)
* [Built With](#built-with)
* [Versioning](#versioning)
* [Authors](#authors)
* [License](#license)
* [Acknowledgements](#acknowledgements)
* [Missing Sections](#missing-sections)
* [Missing Instructions](#missing-instructions)


**Inspired by GitLab, this handbook serves as a transparent statement to our employees, founders, customers, competitors, partners, of our:**
- Values
- Company Information
- Operation manual  
- Strategic objective
- Organization strategy
- Business objectives

## Technologies Used
- Jekyll
- HTML, CSS, JavaScript
- Vue.js
- Bulma
- SASS

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development, maintenance, updating, and testing purposes. See deployment for notes on how to deploy your updated and tested version on the master branch.

### Prerequisites
1. Command line tool (terminal or command prompt(losers))
2. Git installed
3. Text editor
4. GitLab Account
5. GitLab SSH Key Addition
6. GitLab Permissions from Hack It Hour's VP of Engineering

### Installing
A step by step guide to installing what you need:

1. Command Line Tool - Congratulations! It's already installed. Search for 'terminal' on OSX or 'command prompt' on Windows.

2. Make sure Git is installed:
```
$ git --version
git version 2.xx.x (...)
```

  If git is not installed:
  ```
  Linux: (yum & apt-get are package managers)

  sudo yum install git-all
  suod apt-get install git-all
  ```

  To install git:
  https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

  Once installed, recheck for the version of git installed.

3. Text Editor

  Here at Hack It Hour, we use Atom, which you can find and download here:
  ```
  [atom][https://atom.io/]

  You can find documentation on how to use and setup atom here:

  https://atom.io/docs

  Also, @ChrisJohnsoct has made a special guide for you in the handbook under

  Engineering > Tools.
  ```

4. GitLab Account

```
Go to GitLab.com, sign up, let the VP of Engineering know your account name.
```

5. GitLab SSH Key Addition

```
Go to https://docs.gitlab.com/ce/ssh/README.html#generating-a-new-ssh-key-pair to create a new pair of SSH keys on your computer and add them to your GitLab account.

This can be difficult or confusing, so ask a fellow engineer if you need help! They'll get you figured out!
```

6. GitLab Permissions from VP of Engineering

```
Upon creation of your account, you will provide the email and username of your GitLab account to the VP of Engineering to be added to the company's list of company GitLab users to keep a running list of those who should have access to our private repos.
```

### Downloading the Code
Steps:
1. Clone the repository

```
git clone X
```

Find X here:

![Hack It Hour Handbook Clone URL](readme/git-clone-path.png)


## Create a New Branch for Updating
Steps:
1. Create a new local branch to make your personal changes or updates on.

```
git branch [branch-name]
```

2. Change to your new branch (checkout that branch)

```
git checkout [your-branch-name]
```

## Making Changes
Make changes to ONLY the files in the handbook/../ directories. If the file doesn't end in ".md," don't make any changes.

## Creating New Files
1. Create a new file within one of the handbook/ directories ending with ".md".
2. Add a YAML front matter block at the top of the ".md" file:

```
---
layout: page
title: [Title of New Page]
permalink: path/to/new/file (handbook/directory/newfile)
---
```

3. Add content using markdown below the YAML front matter block.

## Ensure remote origin is setup
```
git remote
```
Should show 'origin,' but if it doesn't:
```
git remote add origin [clone-url-X-from-above]
```

## Pushing Changes Back to Repository
```
git push
```
Type in your password for GitLab

## Requesting a Merge Request on GitLab.com
```
Go to the project repository on GitLab.com: https://gitlab.com/Johnsoct/hackithour-handbook

Go to 'Merge Request' in the project menu navigation.

Click the green button, 'New Merge Request.'

Select the Johnsoct/hackithour-handbook as the source project.

Select the branch you created and checked out to make your changes as the source branch.

Select the Johnsoct/hackithour-handbook as the target project.

Select the master branch as the target branch.

Click the green button, 'Compare branches and continue.'

Give the merge request a unique, descriptive title and fill out a description of what you changed or added. Be descriptive and detailed to save time for the person reviewing and approving the merge.

Select any information relevant to the change or updates.

Click the green button, 'Submit merge request.'
```

## Built With
* Jekyll
* Grunt.js

## Versioning
Current version is 1.0.1

## Authors
* **Chris Johnson** - Intial development and continuing updates and usage
* **Thomas Aucott** - Initial development and continuing updates and usage

## License
This project is licensed under the MIT License.

## Acknowledgements
* Kudos to anyone who took the time to add or update this resource - this helps Hack It Hour operate at a much higher level and makes all of our lives and responsibilities easier.

### Missing Sections
* Strategic objective
* Organization strategy
* Business objectives

### Missing Instructions
* instructions for requesting a merge request
