---
layout: home
---
* General
  <!-- * [Values](/handbook/general/value) -->
  * [General Guidelines](/handbook/general/genguidelines)
  <!-- * [Handbook Usage](/handbook/general/handbook-usage) -->
  * [Strategic Objective](/handbook/general/strategic-objective)
  * [Organizational Strategy](/handbook/general/organizational-strategy)
  * [Being a Startup](/handbook/general/being-a-startup)
  <!-- * [Communication](/handbook/general/communication) -->
  <!-- * [Security](/handbook/general/security) -->
  <!-- * [Signing legal Documents](/handbook/general/signing-legal-documents) -->
  <!-- * [Working Remotely](/handbook/general/working-remotely) -->
  <!-- * [Tools and tips](/handbook/general/tools-and-tips) -->
  <!-- * [Leadership](/handbook/general/leadership) -->
  <!-- * [Secret Santa](/handbook/general/secret-santa) -->
  <!-- * [Using Git to Update this Handbook](/handbook/general/git-page-update) -->
* Engineering
  <!-- * [Engineering Guidelines](/handbook/engineering/eng-guidelines) -->
  * [Tools](/handbook/engineering/tools)
  * [Support](/handbook/engineering/support)
  <!-- * [Tasks](/handbook/engineering/tasks) -->
  <!-- * [Infrastructure](/handbook/engineering/infrastructure) -->
  <!-- * [Build](/handbook/engineering/build) -->
  <!-- * [Implementing Design](/handbook/engineering/implementing-design) -->
  <!-- * [Front End](/handbook/engineering/frontend) -->
  <!-- * [Back End](/handbook/engineering/backend) -->
* Finance
  <!-- * [Accounts Receivable](/handbook/finance/accts-rec) -->
  <!-- * [Accounts Payable](/handbook/finance/accts-pay) -->
  <!-- * [Payroll](/handbook/finance/payroll) -->
* Marketing
  <!-- * [Content](/handbook/marketing/content) -->
  <!-- * [Blog](/handbook/marketing/blog) -->
  <!-- * [Markdown Guide](/handbook/marketing/markdown-guide) -->
  <!-- * [Social Marketing](/handbook/marketing/social-marketing) -->
  <!-- * [Social Media Guidelines](/handbook/marketing/sm-guidelines) -->
* People Operations
  <!-- * [Benefits](/handbook/peopleops/benefits) -->
  <!-- * [Incentives](/handbook/peopleops/incentives) -->
  <!-- * [Onboarding](/handbook/peopleops/onboarding) -->
  * [Hiring](/handbook/peopleops/hiring)
  <!-- * [Firing](/handbook/peopleops/firing) -->
  <!-- * [Recommended Equipment](/handbook/peopleops/rec-equipment) -->
* Product
  <!-- * [Product Improvement Engine](/handbook/product/product-imp-engine) -->
  <!-- * [Product Areas](/handbook/product/product-areas) -->
  <!-- * [Data Analysis](/handbook/product/data-analysis) -->
* Sales
  <!-- * [Customer Success](/handbook/sales/customer-success) -->
