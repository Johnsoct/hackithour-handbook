module.exports = function(grunt) {
  grunt.initConfig({
    sass: {
      dist: {
        options: {
          style: 'compressed',
          sourceMap: 'none'
        },
        files: [{
          src: ['src/sass/*.scss'],
          dest: 'dist/css/main.css'
        }]
      }
    },
    postcss: {
      options: {
        map: false,
        processors: [
          require('pixrem')(),
          require('autoprefixer')({browsers:'last 2 versions'}),
          require('cssnano')()
        ]
      },
      dist: {
        src: 'dist/css/*.css'
      }
    },
<<<<<<< HEAD
    qunit: {
      files: ['test/**/*.html']
    },
    jshint: {
      // define the files to lint
      files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
      // configure JSHint (documented at http://www.jshint.com/docs/)
      options: {
        esversion: 6,
        // more options here if you want to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true
        }
=======
    watch: {
      css: {
        files: 'src/sass/**/*.scss',
        tasks: ['sass', 'postcss']
      },
      js: {
        files: 'src/js/*.js',
        tasks: ['uglify', 'jshint']
>>>>>>> development
      }
    },
    uglify: {
      build: {
        src: ['src/js/*.js'],
        dest: 'dist/js/app.min.js'
      }
    },
    jshint: {
      options: {
        esversion: 6
      },
<<<<<<< HEAD
      files: [{
        expand: true,
        src: ['assets/**.{jpg,gif,png}'],
        cwd: 'test/',
        dest: 'dest/assets/'
      }]
=======
      beforeconcat: ['src/js/app.js'],
      afterconcat: 'dist/js/app.js'
    },
    concat: {
      dist: {
        src: ['src/js/app.js'],
        dest: 'dist/js/app.js'
      }
    },
    responsive_images: {
      dev: {
        options: {
          engine: 'im',
          sizes: [
            {
              suffix: '_small',
              rename: false,
              width: 480,
              quality: 60
            }, {
              suffix: '_medium',
              rename: false,
              width: 640,
              quality: 60
            }, {
              suffix: '_large',
              rename: false,
              width: 800,
              quality: 60
            }, {
              suffix: '_extralarge',
              rename: false,
              width: 960,
              quality: 60
            }, {
              suffix: '_landing',
              rename: false,
              width: 1280,
              quality: 60
            }
          ]
        },
        files: [{
          expand: true,
          src: ['/**.{jpg,gif,png}'],
          dest: 'dist/img/'
        }]
      }
>>>>>>> development
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-responsive-images');
<<<<<<< HEAD

  // this would be run by typing 'grunt test' on the command line
  grunt.registerTask('test', ['jshint', 'qunit']);

  // the default task for 'grunt'
  grunt.registerTask('default', ['jshint', 'qunit', 'concat', 'uglify']);
=======

  grunt.registerTask('default', ['jshint', 'concat']);
  grunt.registerTask('w', ['watch']);
  grunt.registerTask('r', ['responsive_images']);

>>>>>>> development

};
