---
layout: page
title: Organization Strategy
permalink: handbook/general/organizational-strategy/
---
## We’re all employees...
Before we continue, I want everyone to think of yourself as an junior of the company. As the founder of Hack It Hour, I expect my COO, CTO, VPs, and managers to act as any junior, which means you speak respectfully to others, you do as you’re told, you ask questions until you understand your tasks, you are honest and transparent with others, you stay busy, stay enthusiastic, and stay helpful.

I want the culture of this company to be open and transparent to benefit the understanding of our customers and ourselves. This doesn’t mean that you can forget formalities when your boss or manager is speaking to you kindly and openly. I’m a product of the United States Marine Corps which means that I respect and believe in order, and I expect you all to as well.

Lastly, below you will find a bunch of fancy, empowering titles. These are not titles, but Positions of Responsibility. They serve to provide a chain of command, when necessary (if necessary), and describe the responsibilities of each members’ accountabilities for Hack It Hour.

* [We're All Employees](#were-all-employees)
* [Position Organization](#position-organization)
* [Responsibility Phases](#responsibility-phases)
* [President and Chief Operating Officer (COO)](#president-and-chief-operating-officer)
* [President and Chief Technology Officer (CTO)](#president-and-chief-technology-officer)
* [Vice-President of Marketing](#vice-president-of-marketing)
* [Vice-President of Engineering](#vice-president-of-engineering)
* [Vice-President of Operations](#vice-president-of-operations)
* [Vice-President of Finance](#vice-president-of-finance)
* [Director of Legal Affairs](#director-of-legal-affairs)
* [Sales Manager](#sales-manager)
* [Advertising/Research Manager](#advertisingresearch-manager)
* [Production Manager](#production-manager)
* [Front End Engineer](#front-end-engineer)
* [Back End Engineer](#back-end-engineer)
* [Designer](#designer)
* [Content Curator and Editor](#content-curator-and-editor)
* [Service Engineer](#service-engineer)

<hr>

## Position Organization
[Back to Top](#were-all-employees)

For the foreseeable future, Hack It Hour will maintain these fifteen positions, but they will be split among three “stacks.”

#### Business, strategy, fundraising, goals, vision:
* Chief Operating Officer
* VP of Operations
* VP of Finance
* Director of Legal Affairs

#### Technical, engineering:
* Chief Technology Officer
* VP of Engineering
* Production Manager
* Front End Engineer
* Back End Engineer

#### Marketing, social, sales, customer, design:
* VP of Marketing
* Sales Manager
* Advertising/Research Manager
* Designer
* Content Curator and Editor
* Service Engineer

## Responsibility Phases
[Back to Top](#were-all-employees)

As we focus on growing, we have a much better chance of success if we simplify our focus on a few responsibilities instead of an entire list.

I’ve broken up each position into three phases, which represent company milestones.

#### Phase 01:
is centered around the absolute basic and most critical responsibilities per each position.

#### Phase 02:
focused on the responsibilities necessary for full-time business operation and growth.

#### Phase 03:
responsibilities crucial to the management of employees and more complex systems.

## President and Chief Operating Officer
[Back to Top](#were-all-employees)

#### Accountability:
* Reports to stakeholders

##### Phase 01:
* Designing and implementing business strategies, plans and procedures
* Establishing policies that promote company culture and vision
* Set comprehensive goals for performance and growth
* Overall achievement of the Strategic Objective
* Overseeing operations of the company and the work of executives
* Assist in fundraising ventures

##### Phase 02:
* Evaluate performance by analyzing and interpreting data and metrics
* Reporting to SHAREHOLDERS, who include, most likely, the members of the LLC (Chris T. Johnson, Chris L. Johnson, and Thomas G. Aucott)
* Make sure the company is capturing relevant data, performing analysis, distill insights, and implementing decisions based on them
  * Data analyst(s) directly report to the COO for most accurate, up-to-date information
  * Product: What features do people want to pay for?
  * Engineering: How do we instrument our product and services?
  * Product Marketing: What is the most effective message?
  * Lead Generation: What channels are most effective?
  * Initial Scale: What free users are most likely to buy?
  * Customer Success: Who do we message what to increase adoption?

##### Phase 03:
* Managing relationships with partners/vendors
* Lead employees to encourage maximum performance and dedication
* Participate in expansion activities (investments, acquisitions, corporate alliances, etc.)



## President and Chief Technology Officer
[Back to Top](#were-all-employees)

#### Accountability:
* Reports to stakeholders

##### Phase 01:
* Technical
  * Develop strategies and leverage technologies to enhance or build the product
    * Sets:
      * Tone
      * Best practices and standards for all of engineering
  * Ensures software quality
  * Handle tough technical decisions
* Managerial
  * Assist CEO where needed and when available
  * Set engineering expectations and measureable progress
* Social
  * Technical evangelist

##### Phase 02:
* Managerial
  * Focus on keeping the core functionality of Hack It Hour in great shape

##### Phase 03:
* Technical
  * Lead engineering team
* Managerial
  * Veto power on all features
  * Advocates for the engineering team and technology base before the CEO, shareholders, and other departments
  * Hiring, firing, recruiting, job assignments of engineering team
  * Builds team morale, keeps people happy, motivated, fit, fed, trained, and focused - Engineering Team Mom!
  * Delegate efficiently and effectively
  * Give timely feedback
* Social
  * Collaborate with vendors



## Vice-President of Marketing
[Back to Top](#were-all-employees)

#### Accountability:
* Reports to the COO

##### Phase 01:
* Lead marketing efforts
* Define our short and long term marketing strategy
* Develop marketing materials that reflect Hack It Hour’s value proposition, advantages and positioning

##### Phase 02:
* Complete analysis of previous marketing efforts
* Manage different customer channels
* Finding new customers
* Finding new ways to provide customers with the satisfactions they derive from Hacks, at:
  * lower costs
  * greater ease

##### Phase 03:
* Work with sales to ensure a smooth handover of criteria and process



## Vice-President of Engineering
[Back to Top](#were-all-employees)

#### Accountability:
* Reports to the CTO and COO

##### Phase 01:
* Support the CTO and product leads in executing the technical vision for Hack It Hour
* Advise potential problems to CTO early
* Ensure hackithour.com is fast, available, and secure

##### Phase 02:
* Be available for quick turnaround time on new tasks
* Tackle cross-disciplinary / cross-team initiatives
* Make sure handbook is used and maintained
* Promote Hack It Hour as a great product and place to work by engaging on social media and writing blog posts
* Reports will include the directors of backend, frontend, infrastructure, and maybe UX and security

##### Phase 03:
* Work across all of engineering teams to deliver quality
* Prioritize work and resources across teams
* Identify positions we need to hire for, open up vacancies, and interview applicants
* Help during recruiting
* Grow skills in team leads and directors, give them sense of progress, and proactively promote people
* Identify and remedy underperformance
* Identify missing skills and organize training to remedy
* Reorganize teams, mostly splitting them up
* Ensure engineers can have career progress without being a manager


## Vice-President of Operations
[Back to Top](#were-all-employees)

#### Accountability:
* Reports to the COO

##### Phase 01:
* Executive in charge of strategy
  * COO’s top strategic adviser
    * Meets often with the COO to suggest improvements or new directions

##### Phase 02:
* Middle man between company’s top executives and managers or directors
  * Must translate the big-picture strategic goals of the top executives to the divisional level of the managers to ensure they accurately understand and effectively implement it
* Expected to find new areas for growth and improvement for each of the company’s divisions or branches in accordance with the overall company strategy
* Oversees the directors and managers as they carry out the plan
  * Holds managers accountable to the executive strategic plan by setting defined goals for each division and the measuring the results against the goals
    * Often involves overcoming resistance from the managers especially when pushing the plans or company in a direction that changes the traditional way the managers are use to doing things



## Vice-President of Finance
[Back to Top](#were-all-employees)

For now and even in the next few years, the VP of Finance will simply be covering the basics of revenue, expenses, balancing the books, payroll, and finance standards. If we get to a point where we can hire someone to specifically handle this position with real experience, we’ll increase the responsibilities to an industry standard for tech and education companies.

#### Accountability:
* Reports to the COO

##### Phase 01:
* Handle payroll
* Implement new standards for processes and functions regarding finance matters upon approval of the COO

##### Phase 02:
* Accounts Receivable
* Accounts Payable
* Supporting both Marketing and Operations in the fulfillment of their accountabilities by:
  * achieving the company’s profitability standards,
  * securing capital whenever needed,
  * and making sure we’re always getting the best rates and costs.


## Director of Legal Affairs
[Back to Top](#were-all-employees)

#### Accountability:
* Reports to the COO

##### Phase 01:
* Lead the effort in defining the Company’s intellectual property strategy including consideration of patents, trademarks and copyrights.
* Coordinating with outside legal counsel in various subject matters. Ensuring the cost-effective use of outside counsel, through the use of preferred providers, technology, alternative fee arrangements, etc.
* Developing a thorough understanding of and familiarity with Hack It Hour’s business, its people, products, technology, markets, facilities, customers and competitors to identify trends and formulate strategies accordingly. Other duties as assigned.

##### Phase 02:
* Coordinating with management, as appropriate, to ensure any and all contractual matters are handled in a timely, efficient, and cost-effect manner and to ensure that processes and procedures will be developed and applied consistently.
* Serving as a trusted advisor and business partner by providing expertise and guidance for the accomplishment of overall business objectives of Hack It Hour such as fostering a culture of innovation, while ensuring full compliance with applicable laws and regulations and avoiding unacceptable risks.

##### Phase 03:
* Reviewing and negotiating related sales contracts with existing and prospective customers.
* Providing strategy, guidance and deal execution for other third party agreements with resellers, in-bound technology partners and potential acquisitions.
* Providing legal expertise and advice on issues arising from actual or anticipated lawsuits. Anticipating and guarding against legal risks facing Hack It Hour.
* As a service provider to other groups within Hack It Hour you will be measured on reducing legal review cycle times, cost reduction, strategic contributions and peer feedback.
* A typical day in the life will include reviewing customer NDA’s, defining and implementing an IP strategy, negotiating several end user license agreements, responding to employee legal issues, managing trademark issues and reviewing open source policy and processes.


## Sales Manager
[Back to Top](#were-all-employees)

##### Accountability:
* Reports to VP of Marketing

##### Phase 01:
* Design and implement a strategic business plan that expands the company’s customer base and ensure it’s strong presence
* Build and promote strong, long-lasting customer relationships by listening with them to understanding their needs
* Cold call, direct email, and perform other lead generation activities
* Develop and follow up on business leads

##### Phase 02:
* Achieve growth and hit sales targets
* Ensure customer satisfaction
* Advise company about sales performance
* Direct distribution of product or service
* Analyze sales statistics gathered to determine sales potential and inventory and staff requirements and to monitor customers’ preferences
* Maintain reports
* Generate numbers for company to determine if sales goals have been met

##### Phase 03:
* Identify emerging markets and market shifts while being fully aware of new products and competition status
* Manage sales support staff and representatives
* Manage team of sales staff
* Assign sales territories, set sales goals, and establish training programs for the company’s sales reps
* Set sales quotas
* Advise sales reps on ways to improve their sales performance
* Develop scripts for sales
* Maintain contact with dealers and distributors
* Travel to make sales calls
* Arrange for travel
* Hire, train, and lead sales reps


## Advertising/Research Manager
[Back to Top](#were-all-employees)

#### Accountability:
* Reports to the VP of Marketing

##### Phase 01:
* Responsible for marketing and publicising goods or services
  * Inform public about our goods and services
* Identify potential markets
* Develops concepts
  * Develop advertising campaigns, including TV ads, website ads, radio ads, pay-per-click, banners, and other internet ads
  * Develop stickers, because stickers
* Outlines goals

##### Phase 02:
* Create direct mail initiatives
* Measure ad effectiveness and optimize if needed
* Work with account executives to come up with ad campaigns
* Approves copywriting
  * Create, review, approve, and revise copy
* Sets budgets for ad campaigns
  * Set budgeting goals
  * Prepare cost estimates for campaigns
  * Oversee media buying
  * Oversee in-house accounts for ad campaigns
  * Approve changes to budget as needed

##### Phase 03:
* Coordinate staff members who create and deliver ads
* Work with sales staff to generate ideas for the campaigns
* Select agencies to partner with
  * Approve agency plans
* Performs research to find target audience
* Oversee creative staff
* Holds focus groups
* Creates Storyboards


## Production Manager
[Back to Top](#were-all-employees)

##### Accountability:
* Reports to the VP of Operations and Engineering

##### Phase 01:
* Works with marketing, sales, engineering, finance, etc. to make the product a business success (usually engineering and marketing)
* Determines what product and features to make and guide instruction with engineering team
* Software program management
  * Work with engineering team
  * Focus on:
    * engineering processes
    * design
    * documentation
    * planning
    * execution
    * operations
    * feedback
* Behave like an ‘embedded CEO’
* Being enthusiastic about the company’s product(s)
* Foster teamwork based on a lean process
* Insist on discipline and keeping commitments

##### Phase 02:
* Leads and manages products from ideation to delivery
* Software product marketing management
  * Focus on Marketing communication activities
* Technical product management
  * Focus on collecting requirement gathering and communication with clients
* Approves the roadmap and content and determines what and how to innovate
* Responsible for the entire value chain of a product following the life cycle
* Taking risks and managing them

##### Phase 03:
* Business success with individual product portfolio
* Drive strategy and portfolio from market and customer value
* Measuring their contribution on sales (top-line) and profits (bottom-line)



## Front-End Engineer
[Back to Top](#were-all-employees)

#### Job Description:
We are looking for a Front-End Web Developer who is motivated to combine the art of design with the art of programming. Responsibilities will include translation of the UI/UX design wireframes to actual code that will produce visual elements of the application. You will work with the UI/UX designer and bridge the gap between graphical design and technical implementation, taking an active role on both sides and defining how the application looks as well as how it works.

#### Accountability:
* Reports to the VP of Engineering and Production Manager

##### Phase 01:
* Build reusable code and libraries for future use
* Ensure the technical feasibility of UI/UX designs
* Implement all responsive designs approved by the Production Manager or VP of Engineering
* Freely claim any task on the tasks list and begin working on it until completed to the satisfactory requirements
* Optimize application for maximum speed and scalability
* Modify and maintain website and application interfaces
* Collaborate with other team members and stakeholders
* Work with back end developers, such as on APIs and more
* Maintain software workflow management with project management tools, such as GitLab or task runners
* Implement SEO best practices
* Testing the site during all stages of development

##### Phase 02:
* Develop new user-facing features
* Code Refactoring
* Improve custom libraries and documentation.
* Manage Git branches and testing of new features, as well as finalizing merge requests on front end implementations and changes
* Adapting to every unpredictable responsibility known to developers


## Back-End Engineer
[Back to Top](#were-all-employees)

#### Job Description:
We are looking for a Back-End Web Developer responsible for managing the interchange of data between the server and the users. Your primary focus will be development of all server-side logic, definition and maintenance of the central database, and ensuring high performance and responsiveness to requests from the front-end. You will also be responsible for integrating the front-end elements built by your coworkers into the application. A basic understanding of front-end technologies is therefore necessary as well.

#### Accountability:
* Reports to the VP of Engineer and Production Manager

##### Phase 01:
* Integration of user-facing elements developed by a front-end developer with server-side logic
* Building reusable code and libraries for future use
* Optimize application for maximum speed and scalability
* Implementation of security and data protection
* Design and implementation of data storage solutions

##### Phase 02:
* Code Refactoring


## Designer
[Back to Top](#were-all-employees)

#### Job Description:
As our new Web designer you will create the look, layout, feel, functionality and features of all the websites we are developing. You will work on developing new websites and on enhancing our existing Web properties.

You will work closely with our creative directors, project managers, strategists and other design team members to develop specifications and make adjustments regarding the use of new and emerging Web technologies.

#### Accountability:
* Reports to the VP of Marketing and Production Manager

##### Phase 01:
* Your key responsibilities will be:
  * Establishing the company’s branding
  * Wireframing
  * Prototype Design
  * Usability Testing
  * Writing clean, semantic code front end code - HTML5, CSS3
* Website Design (Visual design, UI, UX, Interaction)
  * Create high-fidelity mock-ups for vetting and user testing, and finished .psd files for development.
  * Create site layout/user interface by using standard HTML/CSS best practices.
  * Visual design
    * Translate business requirements, user needs, technical requirements into designs that are visually enticing, easy to use, and emotionally engaging
    * Prepare work by gathering information and materials
    * Generate clear ideas, concepts and designs of creative assets from beginning to end
    * Select typography, iconography, color, space, and texture based on visual design principles to help users successfully navigate and become comfortable with Hack It Hour
    * Present the user interface visually so that information is easy to read, easy to understand and easy to find
  * User Interface design:
    * Create, improve and use wireframes, prototypes, style guides, user flows, and effectively communicate your interaction ideas using any of these methods
  * User Experience design:
    * Facilitate business product visions by researching, conceiving, wireframing, sketching, prototyping, and mocking up user experiences for digital products
    * Design and deliver wireframes, user stories, user journeys, and mockups optimized for a wide range of devices and interfaces
    * Make strategic design and user-experience decisions related to core, and new, functions and features
    * Take a user-centered design approach and rapidly test and iterate your designs
  * Interaction design:
    * Creation of experience maps, user scenarios, mockups and flows
* Company Design
  * Develop and maintain robust Webstyle guides for all our consumer- and business-facing products.
  * Assist in the development of a new brand and how to convey it cohesively across our multiple Web properties.

##### Phase 02:
* Company Design
  * Provide thought-leadership on Web design best practices and next-generation digital trends.
* User Interface Design
  * Continually keep yourself and your design and development team updated with the latest changes in your industry’s standards
* User Experience Design
  * Identify design problems and devise elegant solutions
* Interaction Design
  * Creating rapid prototypes to validate design concepts with product managers and stakeholders
  * Staying abreast of UX trends and looking for creative ideas and inspiration in parallel analogous worlds
  * Measuring recently released product features to establish benchmarks and to identify potential areas of improvement
  * Developing interactive reporting dashboards and different types of visualizations
  * Validating ideas and designs using split A/B test and product performance


## Content Curator and Editor
[Back to Top](#were-all-employees)

#### Accountability:
* Reports to the VP of Marketing, Advertising/Research Manager, and Production Manager

##### Phase 01:
* Contribute to the design and planning of content marketing campaigns and strategies
* Develop a consistent Hack It Hour voice guide for other team members to refer to
* Develop a deep understanding of Hack It Hour’s current and upcoming products and features.
* Assist the team to create content assets for content marketing campaigns. Including, but not limited to:
  * Blog posts
  * Social updates

##### Phase 02:
* Maintain a high standard of well written and factually accurate content
* Assist the team in additional content creation assets for content marketing campaigns:
  * Webinars
  * Interviews
  * Newsletters
  * Surveys and Reports

##### Phase 03:
* Sub-edit the contributions of other marketing team members to create a consistent Hack It Hour brand and voice
* Assist the team in additional content creation assets for content marketing campaigns:
  * Whitepapers
  * eBooks



## Service Engineer
[Back to Top](#were-all-employees)

#### Accountability:
* Reports to the VP of Engineering, Marketing, and Operations, Production Manager

##### Phase 01:
* Communicate via email and video conferencing with potential and current clients
* Prepare and provide customer training/preparation, and make the materials widely available

##### Phase 02:
* Engage with our customers — anything from a small advertising firm or a university, to Fortune 100 clients
* Triage customer issues, debug, and find workarounds if possible
* Improve Hack It Hour through customer interaction
* Submit and comment on bug reports and feature requests based on customer interactions
* Create or update documentation based on customer interactions
* Participate in the on-call rotation to provide 24/7 emergency customer response
* Reliably respond to on-call emergencies
* Maintain good ticket performance and satisfaction

##### Phase 03:
* Engage with the development team to escalate bugs, solve problems, or obtain missing information
* Ensure the knowledge we gain from running Hack It Hour.com is shared with customers and users
* Meet or exceed SLA times consistently
