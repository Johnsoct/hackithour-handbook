---
layout: page
title: Strategic Objective
permalink: handbook/general/strategic-objective/
---
## Introduction
A strategic objective is a collection of standards and clear statements of what your business has to ultimately do to achieve the our primary aim. It is not a business plan, but a product of your life plan and your business strategy and plan. It creates the energy by which you will produce results. In the end, it’s a tool for measuring your progress towards a specific end.

* [Money](#standard-money)
* [Opportunity Worth Pursuing](#standard-opportunity-worth-pursuing)
* [Prototype](#standard-prototype)
* [Business Where](#standard-business-where)
* [Business How](#standard-business-how)

## Standard: Money
[Back to Top](#introduction)

#### How big is our vision?
* 2 years = $400,000 gross yearly revenue
* 5 years = $600,000 gross yearly revenue
* 10 years = $1,000,000 gross yearly revenue

#### What will serve your primary aim?
* Thomas:
  * 2 years = $21,000
  * 5 years = $45,000
* Johnson:
  * 2 years = $75,000
  * 5 years = $150,000

#### How much would you like to be able to sell your company for?
* 2 years = (20 x yearly earnings) = $8,000,000
* 5 years = (15 x yearly earnings) = $9,000,000
* 10 years = (10 x yearly earnings) = $10,000,000

## Standard: Opportunity Worth Pursuing
[Back to Top](#introduction)

#### What frustrating experience unique to a large enough group is my company satisfying?
* Designing and Developing sites for humans (end users) not just according to requirements supplied by homework or your client.
* Usability/Information Architecture isn’t taught in formal education and is often a complete failure and therefore, ignored.
* Communication and collaboration with a real client.

#### What kind of business are we?
* Online,real-time educator of web design and development usability that serves to help designers and developers make better products for end users.

#### What is your commodity?
* Online education for web designers and developers.

#### What is your product?
* Joy/Understanding - Experience with a real client, real user, and a realistic, applicable education of applying usability and information architecture to client and user products. The joy of knowing you’re building something that someone really wants and can use.
* Sense of being more connected to humans and those you’re creating for.

#### Who is your customer?
* Designers
* Web Developers

#### What is your customers’ demographic?
* [Demographic Spreadsheet](https://docs.google.com/spreadsheets/d/1bcfD80AB2cF1xTD50T85v9li-iSMjgFP1_vm63WFORM/edit#gid=0)

#### What is your customers’ psychographic?

#### How many selling opportunities do you have (demographics)?

#### How successfully can you satisfy the emotional or perceived need lurking there (psychographic)?

## Standard: Prototype
[Back to Top](#introduction)

#### What is our prototype?
* Hack Sessions
  * Hack Subject(s)
  * Hack session format
  * Hack session guidelines
  * Hack final products (example)
  * Client
    * Participant brief (what to expect)
    * Real or Actor
    * Client Brief (real or actor)
      * Client session length
      * What to expect from participants
      * What is the problem?
      * What type of solution are you looking for?
      * Open to questions
      * Current site specifications
      * Client knows that regardless of hack product, they’ll get their, or have got, solution from Thomas and I.
  * Repository of hacks of various lengths.
* Expected completion date: May 01, 2016

## Standard: Business Where?
[Back to Top](#introduction)

#### Where will we be in business, or where shall we conduct business?
* Online
* Base of Ops in Indiana
* Focus on USA
* Other country participants are allowed as long as:
  * Speak strong English
  * Work on our timezone

## Standard: Business How?
[Back to Top](#introduction)

#### How are you going to be in business?
* Remotely
* Education Service (almost a consulting service)
