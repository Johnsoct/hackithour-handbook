---
layout: page
title: Guidelines
permalink: handbook/general/genguidelines/
---
This handbook is a huge repository of the standards and guidelines that Hack It Hour proudly stands upon; however, that brings us to our biggest concept that every employee must understand.

1. Although there are clear standards for the processes and functionality of Hack It Hour’s company conduct, I highly recommend that you do not take these standards as permanent for the time being. We’re still in the startup phase, so although you’re expected to do things the way they are defined now, don’t complain or resist when we push for a complete change in a standard of a process of function. If there are changes, it’s in the best interest of Hack It Hour and our customers. So, don’t be bitching.

2. Trust wikipedia? We do, because we know it’s reliable. So, this handbook is strongly based on topics from this series on Wikipedia, <a href='https://en.wikipedia.org/wiki/Corporate_law'>Corporate Law</a>.
