---
layout: page
title: Being a Startup
permalink: handbook/general/being-a-startup/
---
To have a successful startup, you need:
* A great idea (including a great market)
* A great team
* A great product
and great execution

## Idea:
What are we building and why?
* We’re building an experience for web designers and developers to learn and experience what it’s like to create for users, to emphasize design and development for users and to listen to users, because anyone can design and develop a feature for a specific set of specifications, but few have been taught the importance and skills to build features for users based off their needs and demands.

Who needs this product?
* Junior and intermediate web designers and developers, especially those who have not worked within companies that expressed user design and development.

Why did we start Hack It Hour?
* As I began getting serious about freelance web development, I began to read more and more about the importance of usability and information architecture to build a product that takes into consideration and performs for a client’s business goals and objectives. This is what needed to do to stand out in the crowd of freelancers. However, there wasn’t any educational content on this besides designers’ blog posts and podcasts that skimmed the topics. Now, I know I’m a developer, so a lot of the sites I used to learn my trade (udemy, treehouse, udacity, codeschool, etc) didn’t emphasize on these topics, but they should have! Designers, I’m sure, learn that the basis of what they do is for the user, but how often do you think designers actually speak with their target audience? Great designers, probably often. Everyone else, probably never. So, I created Hack It Hour to be the source of education for the select who we can teach need to know this information. We’re here to educate ourselves, which simultaneously is our target audience, and increase the quality of products built for users.

How big is our market and what does its growth look like?
* Our developer market: 170,000 @ 26.6% by 2020
* Our designer market: 730,000 @ 1.7% by 2020
* Total: 900,000 @ 28.3% by 2020
* Luckily, our market will continue to grow with the continual growth of the internet, mobile devices, integrated technology solutions, information systems, etc.

## Great Team:
First, what makes a great founder? And more importantly, do you think this is us?
* Unstoppable
* Determined
* Formidable
* Resourceful
* Intelligent
* Passionate
* You want to be someone who is low-stress to work with because you feel “he or she will get it done, no matter what.”
* Common contradictory traits:
  * Rigidity and flexibility
    * Strong beliefs about the core of the company and its mission, but very flexible and willing to learn new things when it comes to almost everything else.
* Incredibly responsive, which is a symbol of:
  * Decisiveness
  * Focus
  * Intensity
  * Ability to get things done

## Great Product:
We have to build a product that at least a small amount of users LOVE not like, because it’s easier to grow the number of users who LOVE your product than to convert users who like your product into users who love your product.

We must build a “product improvement engine” in Hack It Hour. This is a system that involves talking to our users, watching our users use our product, figuring out what parts are sub-par, and making our product better. This cycle never stops. We want to shoot to statistically improve our product X amount each week or month.


## Great Execution:
### Growth and Momentum
Basics:
* Growth and momentum are the keys to great execution
* Growth (as long as positive) solves all problems, and the lack of growth can only be solved by more growth

Growth:
* Winning
* Happiness
* New roles
* New responsibilities
* Career advancement

**Lack of growth and momentum slays founders and, therefore, startups. It causes companies to fail, employees to leave, and business to cease.**

How not to lose momentum:
* Make it your top priority
  * The company does what the CEO measures…
    * It’s valuable to have a single metric that the company optimizes
      * Worth the time to figure out what metric to watch for growth
* Set the bar, the company will follow
* Have a growth/momentum group, inspiration meeting, reminders, etc., * everywhere and often
* Keep a list of all growth and momentum blockers
* Talk as a company about how to grow faster
* Always ask, “Is this the best way to optimize growth” whenever doing anything
  * For ex., a conference may be a lot of fun and motivational, but it’s not really a great way to grow at all (unless you’re manually recruiting users)
* Extreme internal transparency around metrics
  * Direct correlation between how focused on metrics employees are and how well they’re doing
  * Don’t hide metrics!
  * Don’t focus on vanity metrics, only those that matter towards growth and momentum (Ex., signups(vanity) vs retention(quality) )
* Always have an internal “drumbeat” of progress:
  * New features
  * New Customers
  * New hires
  * New or more revenue
  * New milestones or milestone progress
  * New partnerships
  * etc.
* Set SMART goals and celebrate all your wins
* Always talk strategy
  * Share everything you’re hearing from customers with everyone
    * More you share, better you’ll know your customer, better your product will be, and the better off you’ll be

Tips:
* Prepare to logistically handle growth, but don’t panic if things seem to be unraveling
  * Fix it and grow!
* Only plan how things will work at 10x your current scale
* Always have great customer services
  * Passionate users are created by customer service
  * As you grow, you’ll need less support because you’ll know what users struggle with and improve the product in these areas
* Growth may seem small in absolute numbers, but remind your team to look at percentages and that all companies start with low numbers
* Don’t bother with big deals and big press releases
  * Never work
  * Incredibly stressful
  * Lack of attention to what really matters
* Build a great product
* Recruit users manually
* Test growth strategies (ads, referrals, sales, etc.)
* Continue doing more of what works
* One founder has to be good at asking people to use the product and give you money
* Track revenue growth per month

### Focus and Intensity
* We’re in it for the long run and it’s going to be an endurance race, so don’t treat work like an all nighter. Work, go home, eat, spend time with friends, exercise, sleep!
* Be relentlessly focused on your product and growth
* Don’t try to do everything
* Don’t do anything new until you’ve mastered the first thing
* Start with all the conviction on one thing and see it all the way through
* Try 3 major tasks a day and 30 minor ones (similar system)
* The founders should do whatever is important intensely, quickly and decisively
* We have to work on the right things
* When you find something that works, don’t let up and do something else, do more of whatever it is that is working!
* Don’t get caught up in early success, keep working and growing

### Jobs of the CEO (COO and CTO)
* The CEO has to
  * Set the vision and strategy for the company
  * Evangelize the company to everyone
  * Hire and manage the team, especially in areas where he/she has gaps
  * Raise money and make sure the company does not run out of money
  * Set the execution quality bar
  * In addition, find whatever parts of the business you love most and stay engaged there
* If you’re doing your job well,  it’ll be your life to a degree you cannot imagine. Don’t even consider thinking about something else.
* You will have time for one other big thing, whether family, sports, etc., but not much else
  * It’s your job and expectation to maintain a work-life balance to maintain a positive mindset at and about work
* Be super responsive to your team and the outside world
* Always be clear on strategy and priorities
* Show up to everything important
* Execute quickly (especially on decisions that block others from continuing work)
* “Do whatever it takes” attitude
* Things are going to feel broken all the time, but it’s your job to fix them with a smile on your face and reassure your team that it’ll be okay, even if it’s dire
* When unfair, touch situations appear, buckle down and get to work, don’t waste time complaining
* Be the guy who “X just somehow gets things done”
* When you don’t know something, ask ask ask
* Continue learning, connecting with your employees, and managers
* You have to make others think your company is primed to be the most important startup of the decade, but you should be paranoid about everything that could go wrong and be ready to handle anything
* Always, always get to the root of the problem. No bandaids
* Be optimistic
* Create the company with the purpose of giving those under you a reason to do the day-to-day work
* Don’t try to reinvent the wheel for how departments work because this steals time and focus from improving your product (do what already works)

### Hiring and Managing
* According to the YC, the most successful companies have waited a relatively long time to start hiring employees.
  * Employees:
    * Are expensive
    * Add complexity and communication overhead
    * Add inertia
      * Becomes increasingly hard to change directions with a larger team
  * Great people have a lot of opportunities
    * If you’re winning, they’ll come to you and want to work for you
* Be generous with equity, trust, and responsibility
* Go after people you don’t think you can get
* When you’re in the recruiting mode (from when you get product-market fit to T-infinity), you should spend 25% of your time on recruiting
  * CEO handles recruiting
    * Considered the number one time activity by CEOs
* Companies that start with mediocre employees almost never recover, because mediocrity becomes infectious among all employees.
  * Always hire the best and never compromise on hiring
* Value aptitude over experience for almost every role
  * Raw intelligence and a track record of getting things done
  * People you like (you’ll be spending a lot of time together)
* Good managers know not to “go into hero mode” and try and do everything themselves, which makes them unavailable to their staff
* Being late on a project is better than not being a functional team
* Generally speaking, working together in the same office has an atmospheric advantage over startups who work remotely from the beginning
* Fire quickly
  * Fire those who are toxic to the company culture no matter how good they are at what they do
  * Culture is defined by who you hire, fire, and promote

### Competitors
* YC believes 99% of startups die from suicide, not murder
* Worry about all your internal problems
  * If you fail, it will more likely due to failing to make a great product and/or make a great company
  * 99% of the time, ignore competitors
    * Especially when they raise a lot of money, get a lot of publicity, etc.
* Henry Ford, “The competitor to be feared is one who never bothers you at all, but goes on making his own business better all the time.”

### Making Money
* Short story, you have to get users to pay you more than it costs to deliver
* Generally, if you have a paid product with less than a $1000 customer lifetime value (LTV), you generally can’t afford sales
  * Experiment with different user acquisition methods
    * SEO, SEM
    * Ads
    * Mailings
    * etc.
  * Try to repay your customer acquisition cost (CAC) in 3 months
* Strive for “ramen profitability” as quickly as you can
  * Enough money for the founders to live on ramen
  * No longer at the whims of investors and financial markets
* Watch your cash flow obsessively
  * Founders have a way of running out of money without being aware it was happening

### Fundraising
* We should raise money when we need it or it’s available on good terms
* Don’t lose your sense of frugality or to start solving problems by throwing money at them
* Having too much money can be a very bad thing for business
* Secret to successfully raising money is to have a good company
* Investors look for companies who are going to be really successful regardless of whether they invest, but can grow faster with outside capital
* Always be prepared to explain why you could be a huge success
* Fundraising when your company isn’t in good shape to attract capital will burn your reputation and waste time
* Most companies struggle with fundraising, because most companies look bad and unfashionable
* Necessary evil that must be done
* Most VCs don’t know about most industries, so use metrics as convincing material
* Most VCs also require some sort of introduction to be taken seriously
* A pitch requires at least a mission, problem, product, business model, team, market and market growth rate, and financials

