---
layout: page
title: Hiring
permalink: handbook/peopleops/hiring/
---
## Hiring Process
* [COO](#coo)
* [CTO](#cto)
* [VP of Marketing](#vp-of-marketing)
* [VP of Engineer](#vp-of-engineering)
* [Director of Legal Affairs](#director-of-legal-affairs)
* [Front End Engineer](#front-end-engineer)
* [Back End Engineer](#back-end-engineer)
* [Content Curator and Editor](#content-curator-and-editor)
* [Service Engineer](#service-engineer)

<hr>

## COO
[Back to Top](#hiring-process)
#### Requirements
* Experience successfully leading a comparable effort
* Result focus
* Demonstrate use of analytics to double done on successful efforts
* Outstanding written communication skills
* Desire to develop a team
* Ability to partner with other executive team members
* Fundraising experience
* Understanding of business functions of HR, finance, marketing, etc.
* Working knowledge of data analysis and performance/operation metrics
* Working knowledge of IT/business infrastructure and MS Office, or other document processors
* Excellent interpersonal and public speaking skills
* Aptitude in decision-making and problem-solving

## CTO
[Back to Top](#hiring-process)
#### Focus
* Customer
* End product
* Increasing revenue
* Get shit done
* Building the company strength and credibility

#### Interviewing
* A background in software development: degree in computer science, knowledgeable in system architecture, programming and software design
* Role flexibility: needs to be willing to get their hands dirty, but still be able to transition to a managerial role as the company grows
* Knowledge of existing and emerging technologies: be able to evaluate multiple technologies and identify those that are the best fit for the company. Technology selection should come from a deep understanding, a long term vision and playing to the strengths of the team rather than following a trend
* Understand of their limitations: Must be able to identify their limitations and identify experts to fill these gaps
* Knowing who to hire: responsible for hiring the core development team, which means he/she needs to be able to identify and attract developers that aren’t just gifted but will help shape the culture of the company
* … and who to fire: knowing when to fire an employee is just as important as knowing when to hire.
* Create positive workplace culture: fosters a rewarding and positive workplace culture to promote creative work and a contented team. The CTO must ensure to mold the company culture to encourage engagement for talented developers to feel inspired to produce their best work
* Management skills: must be able to manage multiple projects while communicating with the development team and other business units
* Focus on the customer: focuses on both internal and external customers. They naturally understand the needs of the customers and focus on creating products that are customer centric
* Technical evangelist: look for CTOs who embrace blogging and public speaking to become evangelists for their industry. They’re educating the world about technical trends of the future and speaking directly to customers about the company’s value of their products
* Putting in the work: needs to manage expectations, build technology, nurture the company culture and inspiration of his team. Must be willing to go that extra mile and grind it out

## VP of Marketing
[Back to Top](#hiring-process)
#### Goals
* Increase awareness
* Increase engagement on social media (front pages, retweets, blog comments, etc.)

#### Challenges
* Focus of developer marketing (meetups, partnerships, ambassadors, demos, tutorials, events)
* Measure event effectiveness
* Dashboards and funnel measurement
* Email marketing rules of engagement
* Swag store, paid or free?

## VP of Engineering
[Back to Top](#hiring-process)
#### Requirements
* Can handle the above responsibilities
* Lead multiple engineering teams
* Engineering experience
* Potential to lead a 15 person engineering organization

## Director of Legal Affairs
[Back to Top](#hiring-process)
#### Requirements
* Law degree from a top tier US or international law school.
* Minimum 5-7 years of substantial experience in the areas of corporate law in the United States.
* Business acumen, including a well-developed understanding of business and commerce and the ability to diagnose and negotiate corporate legal issues and present positive, creative solutions and alternatives.
* Outstanding interpersonal skills, including diplomacy and flexibility, and the ability to interface effectively and engender trust and confidence with personnel at many different levels throughout Hack It Hour.
* Enthusiasm and "self-starter" qualities enabling him or her to manage responsibilities with an appropriate sense of urgency; the ability to function effectively and efficiently in a fast-paced & dynamic environment.
* Previous experience in a Global Start-up and remote first environment would be ideal.
* Experience with open source software a plus.Experience with employment law a plus.

## Front End Engineer
[Back to Top](#hiring-process)
#### Skills and Qualifications
* Proficient understanding of HTML5 and CSS3
* Basic understanding of server-side CSS pre-processing platforms, such as LESS and SASS
* Proficient understanding of client-side scripting and JavaScript frameworks, including Vue.js
* Good understanding of {{relevant libraries and frameworks}}
* Basic knowledge of image authoring tools, such as Gimp, Photoshop, etc., to make small changes to images, such as crops and resizing.
* Proficient understanding of cross-browser compatibility issues and ways to work around them
* Proficient understanding of Git
* Good understanding of SEO and ensuring applications adhere to it
* Good understanding of Grunt and NPM
* 3 years working within front end development

## Back End Engineer
[Back to Top](#hiring-process)
#### Skills and Qualifications
* Basic understanding of front-end technologies
* Good understanding of LESS and SASS
* Understanding accessibility and security compliance
* User authentication and authorization between multiple systems, servers, and environments
* Integration of multiple data sources and databases into one system
* Management of hosting environment, including database administration and scaling an application to support load changes
* Data migration, transformation, and scripting
* Setup and administration of backups
* Outputting data in different formats
* Understanding differences between multiple delivery platforms such as mobile vs desktop, and optimizing output to match the specific platform
* Creating database schemas that represent and support business processes
* Implementing automated testing platforms and unit tests
* Proficient knowledge of back end languages Python and JavaScript
* Proficient understanding of Git
* Proficient understanding of OWASP security principles
  * [OWASP][https://www.owasp.org/index.php/Main_Page]
  * [Guide][https://github.com/OWASP/DevGuide]
* Understanding of “session management” in a distributed server environment

## Content Curator and Editor
[Back to Top](#hiring-process)
#### Requirements
* A dual-minded approach: Highly creative and an excellent writer/editor but can also be process-driven, think scale, and rely on data to make decisions.
* Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external contributors.
* Extremely detail-oriented and organized, able to meet deadlines.
* Deep understanding of the software development process including git, CI and CD
* Obsessive about content quality not quantity.
* Regular reporting on how content and channel performance to help optimize our content marketing efforts.
* You share our values, and work in accordance with those values.

## Service Engineer
[Back to Top](#hiring-process)
#### Requirements
* Affinity for (and experience with) providing customer support
* Technical Skills
* Able to triage and resolve GitLab issues
* Able to perform complex Linux system administration tasks
* Experience with Ruby on Rails applications and Git
* Communication Skills
* Communicate clearly with customers on technical topics
* Take ownership and work to manage the entire issue lifecycle, from customer, to development team, to resolution
* Makes customers happy
* Excellent spoken and written English

#### Junior Service Engineer
* Junior Service Engineers share the same responsibilities outlined above, but typically join with less or alternate experience in one of the key areas of Service Engineering expertise (SysAdmin skills, Ruby on Rails, Git, and customer service). For example, a person with extensive experience in a web framework other than RoR, but with experience on the other three areas would typically join as a Junior.

#### Senior Service Engineer
* Senior Service Engineers are more experienced engineers who meet the following criteria:
* Technical Skills
  * Can solve most support tickets in a reasonable time without escalating to development
  * Expert in most advanced topics (e.g. LDAP, Jenkins/CI integration, Geo)
  * Deep understanding of GitLab internals and a variety of possible configurations
  * Debug challenging problems
  * Submit merge requests to resolve GitLab bugs
* Leadership
  * Help hire and train new Service Engineers
  * Become a go-to person for the other Service Engineers when they face tough challenges
* Communication
  * Take ownership of improving documentation
  * Lead by example in terms of solving a customer issue, updating documentation as a result, and then radiating this knowledge through a blog post
  * Drive feature requests based on customer interactions
* Suggest and implement improvements to the support workflow
* Contribute to one or more complementary projects

#### Staff Engineer
* A Senior Service Engineer will be promoted to Staff Service Engineer when he/she has demonstrated significant leadership and impact; typically around resolving customer issues. This may involve any type of consistent "above and beyond senior level" performance, for example:
* Regularly submitting merge requests for customer reported/requested GitLab bugs and feature proposals
* Working across functional groups to deliver on projects relating to customer experience and success.
* Writing in-depth documentation and clarifying community communications that share knowledge and radiate Hack It Hour’s technical strengths
* The ability to create innovative solutions that push Hack It Hour’s technical abilities ahead of the curve
* Identifying significant projects that result in substantial cost savings or revenue
* Proactively defining and solving important architectural issues
